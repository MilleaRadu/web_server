const express = require('express')
const Pool = require('pg').Pool
var cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const uuidParse = require('uuid-parse');
// const setTZ = require('set-tz')
// setTZ('UTC')

app.use("/",express.static('public'))
 app.use("/images",express.static(__dirname+"/assets"))
const write_pool = new Pool({
    user : "postgres",
    host:"localhost",
    database : 'inosensing',
    password : "admin",
   
})

const read_pool = new Pool({
    user : "postgres",
    host:"localhost",
    database : 'inosensing',
    password : "admin",
   
})

app.use(cors())
app.use(bodyParser.json())

//evolution of feature id , on layer layer, in 6 hours interval
app.get("/evolution/:layer/:id", async(req,res)=>{
    console.log("found endpoint evolution")
    let id = req.params.id
    // if(req.params.layer == "geohash"){
    //     id = '"'+id+'"'
    // }
    let layer = "caggr_"+req.params.layer+"_2hours"
    
    read_pool.query(`Select * from ${layer} where id =$1 order by two_hour asc`,[id],(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json(error)
        }else{
            res.status(200).json(result.rows)
        }
    })

})

app.get('/testEndpoint', async (req,res)=>{
     read_pool.query('Select * from "replica_test"',(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json(error)
        }else{
            res.status(200).json(result.rows)
        }
     })
})
//user

app.get("/user/:user/:password", async (req,res)=>{
    let username = req.params.user
    let password = req.params.password
    read_pool.query('SELECT * FROM "user" where username = $1 and password = $2',[username,password],(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json(error)
        }else{
            res.status(200).json(result.rows)
        }
    })
})
app.get("/user",async (req,res)=>{
    read_pool.query('SELECT * FROM "user"',(error,result)=>{
        if(error){
            res.status(500).json(error)
        }else{
            res.status(200).json(result.rows)
        }
    })
})



//trip
app.get("/trail/:id/:limit/:offset", async(req,res)=>{
    console.log(req.params.id)
    read_pool.query(`select trail_id , user_id , average_speed , time_started+'3 hour' as time_started ,time_ended,distance,ST_asText(geom) as geom from "trail" where user_id = $1 order by time_started DESC  limit $2 offset $3`,[req.params.id,req.params.limit,req.params.offset],(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            res.status(200).json(result)
        }
    })

})

app.get("/trailNo/:id", async(req,res)=>{
    console.log(req.params.id)
    read_pool.query(`select count(*) from trail where user_id=$1`,[req.params.id],(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            res.status(200).json(result)
        }
    })

})
app.post("/trail",async ( req,res)=>{

    let MULTILINE = "LINESTRING("
    let points = req.body.points
    let pointsArray = points.split(",")
    console.log("points : "+ points)


    let numberOfPoints = req.body.numberOfPoints
    console.log("numberofPoints"+numberOfPoints)
    for( let i = 0 ; i < numberOfPoints;i++){
            //first longitude in postgis
            //array of longitude impar latitude par named points in reqest boddy
            
                MULTILINE = MULTILINE+pointsArray[i]+", "
            
    }
    MULTILINE = MULTILINE.substring(0, MULTILINE.length - 2);
    MULTILINE=MULTILINE+")"

    console.log(MULTILINE)
    write_pool.query('insert into "trail" (trail_id,user_id,geom,average_speed,time_started,time_ended,distance) values($1,$2,$3,$4,$5,$6,$7);',[req.body.trail_id,req.body.user_id,MULTILINE,req.body.average_speed,req.body.time_started,req.body.time_ended,req.body.distance],(error,result)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            res.status(202).json(result)
        }
    })
})


app.post("/user", async ( req,res)=>{
    write_pool.query('INSERT INTO "user" (id,email,forname,surname,username,password) values (uuid_generate_v4(),$1,$2,$3,$4,$5)',[req.body.email,req.body.forname,req.body.surname,req.body.username,req.body.password],(error,results)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            res.status(200).json(results)
        }
    })
})

//set mac adress for specified user
//measurepoint
app.get("/geoPoint/:id", async(req,res)=>{

    read_pool.query('Select datetime,id,temperature,humidity,dust,ST_AsText(geom) as geom from "sensor" where trail_id=$1 order by datetime' ,[req.params.id],(error,results)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            console.log("got points of trail")
            res.status(200).json(results)
        }
    })

})

//userStats
app.get("/UserStats/:id", async(req,res)=>{
    
    let uuid = req.params.id
    console.log(req.params.id)
read_pool.query("select count(trail_id) as trails_no from trail where user_id=$1",[req.params.id],(error,result0)=>{
    if(error){
        console.log(error)
        res.status(500).json(error)
    }else{
                        read_pool.query("select max(average_speed) as max_avg_speed from trail where user_id = $1",[req.params.id],(error,result1)=>{
                        if(error){
                            console.log(error)
                            res.status(500).json(error)
                        }else{
                            read_pool.query("select sum(trail.distance) as total_distance  from trail where user_id=$1",[req.params.id],(error,result2)=>{
                        if(error){

                            // ,max(trail.average_speed) as max_avg_speed,count(trail.*) as trails_no 
                            console.log(error)
                            res.status(500).json({message: "db error)"})
                        }else{
                            console.log("first response : " + Object.keys(result0))

                        read_pool.query("select count(*) as points from sensor where user_id=$1",[req.params.id],(error,result3)=>{
                            if(error){
                                console.log(error)
                                res.status(500).json({message: "db error in second query"})
                            }else{
                                console.log("second response : " + Object.keys(result2))
                                //merge responses and send
                                //response = result+result2
                                let response = {}
                                
                                    

                                    
                                        Object.keys(result0.rows[0]).forEach(key=>{response[key]=result0.rows[0][key]})
                                        Object.keys(result1.rows[0]).forEach(key=>{response[key]=result1.rows[0][key]})
                                        Object.keys(result2.rows[0]).forEach(key=>{response[key]=result2.rows[0][key]})
                                        Object.keys(result3.rows[0]).forEach(key=>{response[key]=result3.rows[0][key]})
                                    

                                res.status(200).json(response)

                            }
                        })
                        }
                    })
                        }
                    })
    }
})

    
    
})

app.post("/geoPoint", async(req,res)=>{

    let geom = "POINT("
    geom=geom.concat(req.body.longitude+' '+req.body.latitude+")")
   
   


    write_pool.query('INSERT INTO "sensor" (user_id,trail_id,id,datetime,geom,dust,humidity,temperature) values($1,$2,uuid_generate_v4(),current_timestamp,$3,$4,$5,$6)',[req.body.user_id,req.body.trail_id,geom,req.body.dust,req.body.humidity,req.body.temperature],(error,results)=>{
        if(error){
            console.log(error)
            res.status(500).json({message: "db error)"})
        }else{
            console.log("got it");
            res.status(200).json(results)
        }
            
    })



});


app.get("/geoPoint", async(req,res)=>{


    read_pool.query('SELECT * FROM "sensor" ', (error, results) => {
        if (error) {
            console.log(error)
          res.status(500).json({message : "error"})
        }else{
            res.status(200).json(results.rows)
        }
        
      })

})



module.exports = app